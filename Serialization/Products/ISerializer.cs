﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Serialization.Products
{
    public interface ISerializer
    {
        void Serialize(object obj, string fileName);
        object Deserialize(string fileName);
        void EncryptAndSerialize(object obj, string filename, string cle);
        object DecryptAndDeserialize(string filename, string cle);
    }
}
