﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Serialization.Products
{
    public class BinarySerializer : ISerializer
    {
        public void Serialize(object obj, string fileName)
        {
            using (var stream = new FileStream(fileName, FileMode.Create))
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, obj);
            }
        }

        public object Deserialize(string fileName)
        {
            using (var stream = new FileStream(fileName, FileMode.Open))
            {
                var formatter = new BinaryFormatter();
                return formatter.Deserialize(stream);
            }
        }

        public void EncryptAndSerialize(object obj, string filename, string cle)
        {
            byte[] salt = Encoding.Default.GetBytes("abcdefgh");
            Rfc2898DeriveBytes keyGenerator = new Rfc2898DeriveBytes(cle, salt);
            byte[] key = keyGenerator.GetBytes(16);
            byte[] iv = keyGenerator.GetBytes(16);

            AesCryptoServiceProvider aesProvider = new AesCryptoServiceProvider();
            ICryptoTransform w = aesProvider.CreateEncryptor(key, iv);

            using (FileStream fs = File.Open(filename, FileMode.Create))
            {
                var serializer = new BinaryFormatter();
                using (CryptoStream cs = new CryptoStream(fs, w, CryptoStreamMode.Write))
                {
                    serializer.Serialize(cs, obj);
                }
            }
        }

        public object DecryptAndDeserialize(string filename, string cle)
        {
            byte[] salt = Encoding.Default.GetBytes("abcdefgh");
            Rfc2898DeriveBytes keyGenerator = new Rfc2898DeriveBytes(cle, salt);
            byte[] key = keyGenerator.GetBytes(16);
            byte[] iv = keyGenerator.GetBytes(16);

            AesCryptoServiceProvider aesProvider = new AesCryptoServiceProvider();
            ICryptoTransform w = aesProvider.CreateDecryptor(key, iv);

            using (FileStream fs = File.Open(filename, FileMode.Open))
            {
                var serializer = new BinaryFormatter();
                using (CryptoStream cs = new CryptoStream(fs, w, CryptoStreamMode.Read))
                {
                    return serializer.Deserialize(cs);
                }
            }
        }
    }
}
