﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Data;
using Serialization.Factory;

namespace Serialization
{
    internal class Program
    {
        private Dossier genDossier()
        {
            Dossier racine = new Dossier("Root");
            Dossier d2 = new Dossier("TEST1");
            racine.ajouterDossier(d2);
            racine.ajouterDossier(new Dossier("TEST2"));
            d2.ajouterDossier(new Dossier("TEST3"));

            d2.ajouterContact(new Contact("nom", "prenom", "couriel", "societe", Link.Friend));
            d2.ajouterContact(new Contact("nom2", "prenom", "couriel", "societe", Link.Network));

            racine.ajouterContact(new Contact("nom3", "prenom3", "couriel", "societe", Link.Acquaintance));

            return racine;
        }

        public void afficher(Dossier dossier)
        {
            Stack<(Dossier, int)> pile = new Stack<(Dossier, int)>();
            pile.Push((dossier, 0));

            while (pile.Count != 0)
            {
                var (courant, position) = pile.Pop();

                string tab = "";
                for (int i = 0; i < position; i++) tab += "\t";

                Console.WriteLine($"{tab}[D] {courant}");
                courant.GetContacts()?.ForEach(x => Console.WriteLine($"{tab}|\t[C] {x}"));

                courant.GetDossiers()?.ForEach(x => pile.Push((x, position + 1)));
            }
        }

        void exemple_serialization()
        {
            Dossier dossier = genDossier();

            var serializer_binary = SerializerFactory.CreateSerializer(SerializationType.Binary);
            serializer_binary.Serialize(dossier, "file.bin");

            Dossier deserializedObject = (Dossier)serializer_binary.Deserialize("file.bin");
            afficher(deserializedObject);

            var serializer_xml = SerializerFactory.CreateSerializer(SerializationType.Xml);
            serializer_xml.Serialize(dossier, "file.xml");

            Dossier deserializedObject2 = (Dossier)serializer_xml.Deserialize("file.xml");
            afficher(deserializedObject2);
        }

        void exemple_chiffrement()
        {
            Dossier dossier = genDossier();

            try
            {
                var serializer_xml = SerializerFactory.CreateSerializer(SerializationType.Xml);
                serializer_xml.EncryptAndSerialize(dossier, "file_encrypted.xml", "clé de chiffrement !!!");

                Dossier deserializedObject2 = (Dossier)serializer_xml.DecryptAndDeserialize("file_encrypted.xml", "clé de chiffrement !!!");
                afficher(deserializedObject2);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine("Erreur lors du chiffrement ou du déchiffrement :(");
            }

            try
            {
                var serializer_binary = SerializerFactory.CreateSerializer(SerializationType.Binary);
                serializer_binary.EncryptAndSerialize(dossier, "file_encrypted.bin", "clé de chiffrement 2 !!!");

                Dossier deserializedObject2 = (Dossier)serializer_binary.DecryptAndDeserialize("file_encrypted.bin", "clé de chiffrement 2 !!!");
                afficher(deserializedObject2);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine("Erreur lors du chiffrement ou du déchiffrement :(");
            }
        }

        static void Main(string[] args)
        {
            Program p = new Program();

            //p.exemple_serialization();

            p.exemple_chiffrement();

            Console.ReadLine();
        }
    }
}
