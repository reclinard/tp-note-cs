﻿using Data;
using Serialization.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serialization.Factory
{
    public static class SerializerFactory
    {
        public static ISerializer CreateSerializer(SerializationType type)
        {
            switch (type)
            {
                case SerializationType.Binary:
                    return new BinarySerializer();
                case SerializationType.Xml:
                    return new XmlSerializer<Dossier>();
                default:
                    throw new ArgumentException("Invalid Serialization Type");
            }
        }
    }

    public enum SerializationType
    {
        Binary,
        Xml
    }
}
