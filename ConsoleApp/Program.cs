﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Serialization.Factory;

namespace ConsoleApp
{
    internal class Program
    {
        private Dossier racine;
        private Dossier dossier_courant;


        /*
         * Transforme une chaîne de caractère en Link
         */
        Link? stringToLien(string str)
        {
            Link? lien;

            switch (str.ToLower())
            {
                case "colleague":
                case "collegue":
                case "c":
                    lien = Link.Colleague; 
                    break;

                case "friend":
                case "ami":
                case "f":
                    lien = Link.Friend; 
                    break;

                case "network":
                case "reseau":
                case "réseau":
                case "n":
                    lien = Link.Network;
                    break;

                case "acquaintance":
                case "connaissance":
                case "a":
                    lien = Link.Acquaintance; 
                    break;

                default:
                    lien = null;
                    break;
            }

            return lien;
        }


        /*
         * Affiche l'aide
         */
        void help()
        {
            Console.WriteLine("Help : ");
            Console.WriteLine();
            Console.WriteLine("afficher");
            Console.WriteLine("creer-dossier  (alias : crd)");
            Console.WriteLine("creer-contact  (alias : crc)");
            Console.WriteLine();
            Console.WriteLine("sauvegarder-xml [file.xml] [clé de chiffrement] (alias : sxml)");
            Console.WriteLine("sauvegarder-bin [file.bin] [clé de chiffrement] (alias : sbin)");
            Console.WriteLine("importer fichier.[xml/bin]");
            Console.WriteLine();
            Console.WriteLine("cd `id` : se déplacer dans le dossier `id`");
            Console.WriteLine("help");
            Console.WriteLine("sortir");
            Console.WriteLine();
        }


        /*
         * Affiche l'arborescence des dossiers
         */
        void afficher()
        {
            Stack<(Dossier, int)> pile = new Stack<(Dossier, int)>();
            pile.Push((racine, 0));

            while (pile.Count != 0)
            {
                var (courant, position) = pile.Pop();

                string tab = "";
                for (int i = 0; i < position; i++) tab += "\t";

                Console.WriteLine($"{tab}[D] {courant}");
                courant.GetContacts()?.ForEach(x => Console.WriteLine($"{tab}|\t[C] {x}"));

                courant.GetDossiers()?.ForEach(x => pile.Push((x, position + 1)));
            }
        }


        /*
         * Créé un dossier
         */
        void creer_dossier(string nom)
        {
            Dossier d = new Dossier(nom);
            dossier_courant.ajouterDossier(d);
            dossier_courant = d;
        }


        /*
         * Créé un contact
         */
        void creer_contact(string nom, string prenom, string couriel, string societe, Link lien)
        {
            Contact c = new Contact(nom, prenom, couriel, societe, lien);
            dossier_courant.ajouterContact(c);
        }

        /*
         * Déplace le dossier courant vers le dossier d'`id` correspondant
         */
        void cd(int id)
        {
            Dossier dossier = null;

            bool trouve = false;
            Stack<Dossier> pile = new Stack<Dossier>();
            pile.Push(racine);

            while (!trouve && pile.Count != 0)
            {
                Dossier courant = pile.Pop();

                if (courant.Id == id)
                {
                    trouve = true;
                    dossier = courant;
                }
                else
                {
                    courant.GetDossiers().ForEach(x => pile.Push(x));
                }
            }

            if (trouve)
            {
                dossier_courant = dossier;
            }
            else
            {
                Console.WriteLine($"Dossier {id} inconnu");
            }
            Console.WriteLine($"Nouveau dossier courant : {dossier_courant.Id} {dossier_courant.Nom}");
        }

        /*
         * Fonction principale du programme
         */
        void lancer()
        {

            help();

            bool fin = false;
            while (!fin)
            {
                Console.Write($"{dossier_courant.Id} {dossier_courant.Nom} > ");
                string input = Console.ReadLine();
                //Console.WriteLine();
                string[] input_array;

                input_array = input.Trim().Split(' ');
                if (input_array.Length == 0)
                {
                    Console.WriteLine("Nombre d'arguments insuffisants");
                    continue;
                }

                switch (input_array[0].ToLower())
                {
                    case "afficher":
                        afficher();
                        break;

                    case "creer-dossier":
                    case "crd":
                        if (input_array.Length < 2)
                        {
                            Console.WriteLine("Nombre d'arguments insuffisants");
                            continue;
                        }
                        creer_dossier(input_array[1]);
                        Console.WriteLine($"Dossier {input_array[1]} créé");
                        break;

                    case "creer-contact":
                    case "crc":
                        if (input_array.Length < 5)
                        {
                            Console.WriteLine("Nombre d'arguments insuffisants");
                            continue;
                        }

                        Link? lien = stringToLien(input_array[5]);

                        if (lien == null)
                        {
                            Console.WriteLine("Lien non reconnu (Colleague, Friend, Network, Acquaintance)");
                            continue;
                        }

                        creer_contact(input_array[1], input_array[2],
                            input_array[3], input_array[4], (Link)lien);
                        Console.WriteLine($"Contact {input_array[1]} {input_array[2]} créé");
                        break;

                    case "cd":
                        if (input_array.Length < 2)
                        {
                            Console.WriteLine("Nombre d'arguments insuffisants");
                            continue;
                        }

                        int id;
                        try
                        {
                            id = int.Parse(input_array[1]);
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Erreur sur `id`");
                            continue;
                        }

                        cd(id);
                        break;

                    case "sortir":
                        fin = true;
                        break;

                    case "sauvegarder-xml":
                    case "sxml":
                        string nom_fichier = "file.xml";

                        if (input_array.Length >= 2)
                        {
                            nom_fichier = input_array[1];
                        }

                        if (input_array.Length >= 3)
                        {
                            // Si on a le paramètre optionnel qui servira de clé de chiffrement
                            Console.WriteLine($"Le fichier sera chiffré avec la clé : {input_array[2]}");

                            try
                            {
                                var serializer_xml = SerializerFactory.CreateSerializer(SerializationType.Xml);
                                serializer_xml.EncryptAndSerialize(racine, nom_fichier, input_array[2]);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.ToString());
                                Console.WriteLine("Erreur lors du chiffrement ou de la sérialisation :(");
                                continue;
                            }
                        }
                        else
                        {
                            // Si on n'a pas le paramètre, on ne chiffre pas
                            try
                            {
                                var serializer_xml = SerializerFactory.CreateSerializer(SerializationType.Xml);
                                serializer_xml.Serialize(racine, nom_fichier);
                            }
                            catch
                            {
                                Console.WriteLine("Erreur sérialisation :(");
                                continue;
                            }
                        }

                        Console.WriteLine($"Sauvegarde dans {nom_fichier} effectuée");

                        break;

                    case "sauvegarder-bin":
                    case "sbin":
                        nom_fichier = "file.bin";

                        if (input_array.Length >= 2)
                        {
                            nom_fichier = input_array[1];
                        }

                        if (input_array.Length >= 3)
                        {
                            // Si on a le paramètre optionnel qui servira de clé de chiffrement
                            Console.WriteLine($"Le fichier sera chiffré avec la clé : {input_array[2]}");

                            try
                            {
                                var serializer_binary = SerializerFactory.CreateSerializer(SerializationType.Binary);
                                serializer_binary.EncryptAndSerialize(racine, nom_fichier, input_array[2]);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.ToString());
                                Console.WriteLine("Erreur lors du chiffrement ou de la sérialisation :(");
                                continue;
                            }
                        }
                        else
                        {
                            // Si on n'a pas le paramètre, on ne chiffre pas
                            try
                            {
                                var serializer_binary = SerializerFactory.CreateSerializer(SerializationType.Binary);
                                serializer_binary.Serialize(racine, nom_fichier);
                            }
                            catch
                            {
                                Console.WriteLine("Erreur sérialisation :(");
                                continue;
                            }
                        }

                        Console.WriteLine($"Sauvegarde dans {nom_fichier} effectuée");

                        break;

                    case "importer":

                        if (input_array.Length < 2)
                        {
                            Console.WriteLine("Nombre d'arguments insuffisants");
                            continue;
                        }

                        string fichier = input_array[1];

                        int ancien_nombre = Dossier.nombre;

                        

                        try
                        {
                            string cle = null;
                            if (input_array.Length >= 3)
                            {
                                // Si on a le paramètre optionnel qui servira de clé de déchiffrement
                                Console.WriteLine($"Le fichier sera déchiffré avec la clé : {input_array[2]}");
                                cle = input_array[2];
                            }

                            string extension = fichier.Split('.').Last();
                            Dossier deserializedObject;

                            if (extension == "xml")
                            {
                                Dossier.nombre = 0;
                                var serializer_xml = SerializerFactory.CreateSerializer(SerializationType.Xml);

                                if (cle != null)
                                {
                                    deserializedObject = (Dossier)serializer_xml.DecryptAndDeserialize(fichier, cle);
                                }
                                else
                                {
                                    deserializedObject = (Dossier)serializer_xml.Deserialize(fichier);
                                }
                                
                            }
                            else if (extension == "bin")
                            {
                                Dossier.nombre = 0;
                                var serializer_binary = SerializerFactory.CreateSerializer(SerializationType.Binary);

                                if (cle != null)
                                {
                                    deserializedObject = (Dossier)serializer_binary.DecryptAndDeserialize(fichier, cle);
                                }
                                else
                                {
                                    deserializedObject = (Dossier)serializer_binary.Deserialize(fichier);
                                }
                            }
                            else
                            {
                                Console.WriteLine($"Extension {extension} non reconnue.");
                                continue;
                            }

                            racine = deserializedObject;
                            dossier_courant = racine;
                        }
                        catch
                        {
                            Console.WriteLine("Erreur désérialisation :(");
                            Dossier.nombre = ancien_nombre;
                            continue;
                        }

                        Console.WriteLine($"{fichier} importé avec succès.");

                        break;

                    case "help":
                        help();
                        break;

                    default:
                        Console.WriteLine("Commande non reconnue :(");
                        break;
                }
                Console.WriteLine();
            }

        }

        /* 
         * Constructeur qui permet d'instancier le dossier racine
         */
        Program()
        {
            racine = new Dossier("Root");
            dossier_courant = racine;
        }

        /*
         * Point d'entrée du programme
         */
        static void Main(string[] args)
        {
            Program p = new Program();

            p.lancer();
        }
    }
}
