﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    [Serializable]
    public class Contact
    {
        public static int nombre;
        public int Id { get; set; }

        public string Nom { get; set; }

        public string Prenom { get; set; }

        public string Couriel { get; set; }

        public string Societe { get; set; }

        public Link Lien { get; set; }

        public DateTime Date_creation { get; set; }

        public DateTime Date_modification { get; set; }

        public override string ToString()
        {
            return $"{Nom} {Prenom} {Couriel} {Societe} {Lien} {Date_creation} {Date_modification}";
        }

        public Contact(string nom, string prenom, string couriel, string societe, Link lien)
        {
            Nom = nom;
            Prenom = prenom;
            Couriel = couriel;
            Societe = societe;
            Lien = lien;
            Date_creation = DateTime.Now;
            Date_modification = DateTime.Now;
            Id = nombre;
            nombre++;
        }

        public Contact() : this("", "", "", "", Link.Acquaintance)
        {

        }
    }

    public enum  Link { Colleague, Friend, Network, Acquaintance }
}
