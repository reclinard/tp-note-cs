﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    [Serializable]
    public class Dossier
    {
        public static int nombre = 0;

        public int Id { get; }
        public string Nom {  get; set; }

        public DateTime Date_creation { get; set; }

        public DateTime Date_modification { get; set; }

        public List<Dossier> Dossiers_enfants;
        public List<Contact> Contacts_enfants;

        public Dossier(string nom, DateTime date_creation, DateTime date_modification)
        {
            Nom = nom;
            Date_creation = date_creation;
            Date_modification = date_modification;
            Id = nombre;
            nombre++;

            Dossiers_enfants = new List<Dossier>();
            Contacts_enfants = new List<Contact>();
        }

        public Dossier(string nom) : this(nom, DateTime.Now, DateTime.Now)
        {

        }

        public Dossier() : this("", DateTime.Now, DateTime.Now)
        {

        }

        public void ajouterDossier(Dossier d)
        {
            Dossiers_enfants.Add(d);
            Date_modification = DateTime.Now;
        }

        public void supprimerDossier(Dossier d)
        {
            Dossiers_enfants.Remove(d);
            Date_modification = DateTime.Now;
        }

        public List<Dossier> GetDossiers()
        {
            return Dossiers_enfants;
        }

        public void ajouterContact(Contact c)
        {
            Contacts_enfants.Add(c);
            Date_modification = DateTime.Now;
        }

        public void supprimerContact(Contact c)
        {
            Contacts_enfants.Remove(c);
            Date_modification = DateTime.Now;
        }

        public List<Contact> GetContacts()
        {
            return Contacts_enfants;
        }


        public override string ToString()
        {
            return $"{Id} - {Nom} {Date_creation} {Date_modification}";
        }

    }
}
