﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class Arborescence
    {

        private Dossier racine;

        public Arborescence() 
        {
            racine = new Dossier ("Root");
        }

        public void ajouterDossier(int id_dossier_parent, Dossier d)
        {
            Dossier dossier = chercher(id_dossier_parent);

            if (dossier != null)
            {
                dossier.ajouterDossier(d);
            }
        }

        public void supprimerDossier(int id_dossier_parent, Dossier d)
        {
            Dossier dossier = chercher(id_dossier_parent);

            if (dossier != null)
            {
                dossier.supprimerDossier(d);
            }
        }

        public void ajouterContact(int id_dossier_parent, Contact c)
        {
            Dossier dossier = chercher(id_dossier_parent);

            if (dossier != null)
            {
                dossier.ajouterContact(c);
            }
        }

        public void supprimerContact(int id_dossier_parent, Contact c)
        {
            Dossier dossier = chercher(id_dossier_parent);

            if (dossier != null)
            {
                dossier.supprimerContact(c);
            }
        }

        public List<Contact> recupererContacts(int id_dossier)
        {
            Dossier dossier = chercher(id_dossier);

            if (dossier != null)
            {
                return dossier.GetContacts();
            }
            else
            {
                return null;
            }
        }

        public List<Dossier> recupererDossiers(int id_dossier)
        {
            Dossier dossier = chercher(id_dossier);

            if (dossier != null)
            {
                return dossier.GetDossiers();
            }
            else
            {
                return null;
            }
        }

        private Dossier chercher(int id_dossier)
        {
            Dossier dossier = null;

            bool trouve = false;
            Stack<Dossier> pile = new Stack<Dossier>();
            pile.Push(racine);

            while (!trouve && pile.Count != 0)
            {
                Dossier courant = pile.Pop();

                if (courant.Id == id_dossier)
                {
                    trouve = true;
                    dossier = courant;
                }
                else
                {
                    courant.GetDossiers().ForEach(x => pile.Push(x));
                }
            }

            return dossier;
        }

        public Dossier getRacine()
        {
            return racine;
        }


    }
}
